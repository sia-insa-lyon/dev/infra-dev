#!/bin/bash
set -e
echo "check: Docker"
docker ps
echo "check: kubectl"
kubectl version --client
echo "check: helm"
helm version
echo "check: k3d"
k3d version
set +e

echo "création du cluster sur le port 8081"
k3d cluster create --api-port 6550 -p "80:80@loadbalancer"
export KUBECONFIG="$(k3d kubeconfig get k3s-default)"

