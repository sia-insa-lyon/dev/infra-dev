# Cluster de dev
Il vous faut les commandes suivantes:
* [k3d](https://github.com/rancher/k3d)
* kubectl
* helm
* docker

Attention, quand vous installez docker, il faut après ajouter votre compte au groupe docker:

```
sudo usermod -aG docker "$(whoami)"
```

Vous devriez aussi installer [Lens](https://k8slens.dev/), pour mieux voir votre cluster. Et si vous êtes connecté au VPN de l'INSA, il faut vous déconnecter et relancer docker, car ça provoque des conflits de routes IP. Si vous en avez vraiment besoin vous pouvez supprimer toutes les routes privées de l'interface `vpn0` Cisco VPN.

## 1er lancement

Rendre les fichiers excutable avec la commande :

```
chmod +x <file_name>
```
Puis
```
sh startme.sh
```
connectez-vous au cluster depuis Lens, quand c'est bon, faites
```
sh install-keycloak.sh
```
Et observez Keycloak se créer.
L'Ingress Controller (traefik) sera disponible sur `localhost:80`

ATTENTION, tout le déploiement du cluster **prend du temps**, surtout si vous avez une mauvaise connexion internet, donc vous aurez des erreurs 503 sur les applications duc lister jusqu'à ce qu'elles aient fini de se lancer.

Utilisez Lens pour voir les étapes de déploiement des applis (création des objets Kube, téléchargement des images Docker, démarrage des bases de données et des applis ...)

## Utilisation du cluster
Vous pouvez récupérer la Kubeconfig avec ``k3d kubeconfig get k3s-default``.

### Keycloak
Keycloak est disponible à l'adresse [http://sso.172.17.0.1.nip.io/auth/](http://sso.172.17.0.1.nip.io/auth/), avec comme login `admin` et mot de passe `admin`.

IMPORTANT: à sa création, Keycloak charge une configuration depuis le fichier `asso-insa-lyon.json`. Ce fichier a été généré depuis un export, mais avec une subtilité : Keycloak n'exporte pas les *secrets/credentials*, par contre il peut les importer. Donc si vous modifiez ce fichier, remettez `72049bdb-3be3-41cc-aaa6-89692d52dc6e` à la place des étoiles dans la partie du Client `adhesion-api`.
### Adhésion
Les fichiers nécessaires à l'installation sont dans le repo BdEINSALyon/Adhésion/api, dans la branche dev-ng (pour l'instant)

Le script `install-adhesion.sh` va effetuer tout ça pour vous

Vous devrez ensuite exécuter la commande `./manage.py seeddb` dans le conteneur 'backend' d'adhésion-api
# Documentation
## Aled jsuis perdu c koi kubernatisse ?
[k8s en 5min, vidéo](https://www.youtube.com/watch?v=PH-2FfFD2PU)
[https://www.digitalocean.com/community/tutorials/an-introduction-to-kubernetes](https://www.digitalocean.com/community/tutorials/an-introduction-to-kubernetes)

et l'Ingress (et Controller) : [vidéo](https://www.youtube.com/watch?v=izWCkcJAzBw) attention, dans cette vidéo il parle souvent de ré-écriture d'URL, mais dans la plupart de nos applications les URLs ne sont pas ré-écrites

fondamentalement, l'Ingress Controller est un *reverse proxy HTTP* qui s'auto(re)configure dynamiquement en lisant les objets *Ingress* du cluster
## Si vous connaissez déjà k8s
dans ce cluster, le *master* et le *worker* sont la même *node*. l'ingress est géré de base avec Traefik et il y a un load-balancer dont l'IP "publique" est celle de son conteneur docker

## Explications
Ce mini-cluster tourne dans Docker, pour être plus pratique à gérer. Sur ma machine j'estime à 1.1Gio l'utilisation de la RAM. Si vous utilisiez *minikube*, vous auriez la perte de performance et de ressources de la virtualisation, et si vous utilisiez *kubeadm*, bonne chance pour lancer celui d'Ubuntu avec moins de 4Go de RAM !

En général, les clusters Kubernetes s'étendent sur plusieurs machines et appliquent une stricte séparation des tâches: les *nodes* (machines) s'occupent de faire *tourner les logiciels*, tandis que des éléments externes fournis par le Cloud vous font le *stockage* et le *réseau d'interconnexion*.

Sur ce mini-cluster, il n'y a pas de réseau entre nodes puisqu'il n'y en a qu'une, et il n'y a pas de stockage externe car c'est géré à l'intérieur du cluster.

### DNS
Vu que vous n'avez (probablement) pas de nom de domaine qui pointe sur votre machine, on peut utiliser le service `nip.io`, qui permet d'avoir une infinités de noms de domaines qui pointent sur l'IP qu'on veut. Par exemple on peut faire
* *sso.172.17.0.1.nip.io*
* *adhesion.172.17.0.1.nip.io*
mais aussi *portailva.172.20.0.2.nip.io* (remplacez par l'IP du load-balancer : ``docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' k3d-k3s-default-serverlb``

#### Pourquoi cette IP bizarre ???
En fait, `172.17.0.1` correspond (par défaut) à l'IP de votre machine dans le réseau virtuel de Docker. Pour vous, `172.17.0.1` est équivalent à `localhost` ou `127.0.0.1`.

Cela permet d'utiliser la même adresse à l'intérieur et en dehors du cluster, tout comme dans le vraie infra !

#### Problèmes de DNS
Si vous n'arrivez pas à joindre `172.17.0.1.nip.io` sur votre machine, c'est sûrement une protection contre les attaques DNS Rebind.
Il faut autoriser `nip.io` dans la whitelist du routeur (possible sur Box fibre SFR)

## Différences avec le Kluster de production chez OVH
Tout d'abord, il n'y a qu'une seule *node*, et pas d'IP publique. L'Ingress Controller ne fait pas de SSL, et les *PersitentVolumeClaim*s ne peuvent pas être crées dans le mode `ReadWriteMany`.

## Similarités
En théorie, ce petit cluster `k3d` émule toutes les fonctionnalités d'un vrai cluster Kubernetes de production, au moins suffisamment pour tester toute l'API Kubernetes.
