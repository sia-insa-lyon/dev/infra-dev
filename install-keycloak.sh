#!/bin/bash
kubectl create namespace keycloak
kubectl -n keycloak apply -f keycloak-h2pvc.yaml
kubectl -n keycloak create secret generic realm-secret --from-file=asso-insa-lyon.json
helm install -n keycloak  --values values-keycloak.yaml keycloak-dev codecentric/keycloak
